require 'Qt'

system '/home/smf/.gem/ruby/2.5.0/bin/rbuic4 untitled.ui -o gui.rb'
data = File.read('gui.rb').gsub('attr_reader', 'attr_accessor')
File.write('gui.rb', data);

require_relative 'gui.rb'

a = Qt::Application.new(ARGV)
u = Ui_Form.new
w = Qt::Widget.new
u.setupUi(w)
w.show

names = []
names_num = 500
File.foreach('male').with_index do |line, line_index|
  names << line;
  if(line_index == names_num) then break end;
end
File.foreach('female').with_index do |line, line_index|
  names << line;
  if(line_index == names_num) then break end;
end

names = names.sort.unshift("")

u.name_slider.maximum = names.size-1

a.connect(u.name_slider, SIGNAL('valueChanged(int)')) do
  u.name_box.text = names[u.name_slider.value]
end

a.exec
